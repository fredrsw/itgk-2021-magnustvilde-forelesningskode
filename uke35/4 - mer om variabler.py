'''Mer om variabler'''

# lag tre forskjellige variabler, x, y og z, på en linje - sett de til tallverdier
x, y, z = 6, 7.0, 2
print(f'x: {x}\ny: {y}\nz: {z}')


# legg til verdien til z i x

x += z
print(f'Oppdatert x: {x}')

# bytt innholdet til x og y
'''
print(f'Før: x = {x}, y = {y}')
temp = x
x = y
y = temp
print(f'Etter: x = {x}, y = {y}')
'''

print(f'Før: x = {x}, y = {y}')
x, y = y, x
print(f'Etter: x = {x}, y = {y}')

# lag tre variabler, a, b og c, som skal ha samme verdi

a = b = c = 0
b+=1
c+=2
print(f'a: {a}\nb: {b}\nc: {c}')